$(document).ready(function () {
    addEventListener();
});

function addEventListener() {
    $("#btn-sign-up").on("click", function () { onBtnSignUpClick(); });
    $("#btn-sign-in").on("click", function () { onBtnSignInClick(); });
    $("#btn-hello").on("click", function () { onBtnHelloClick(); });
}

function onBtnSignUpClick() {
    let user = {
        username: $("#username").val(),
        password: $("#password").val()
    }
    createUser(user);
}

function onBtnSignInClick() {
    let user = {
        username: $("#username").val(),
        password: $("#password").val()
    }
    signIn(user);
}

function onBtnHelloClick() {
    fetch("http://localhost:8080/hello")
        .then(response => response.json())
        .then(data => {
            console.log(data);
        })
        .catch(err => console.log(err));
}

async function createUser(user) {
    try {
        console.log("Creating user");
        console.log(user);
        let response = await fetch("http://localhost:8080/register",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(user),
            });
        let result = await response.json();
        console.log(result);
        alert("Sign up successfully");
    } catch (error) {
        console.assert(error);
        console.assert(response.json());
    }
}

async function signIn(user) {
    try {
        console.log("Signing in...");
        let response = await fetch("http://localhost:8080/login",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(user),
            });
        let result = await response.text();
        console.log(result);
        alert("Sign in successfully");
    } catch (error) {
        console.log(error);
        //console.assert(response.json());
    }
}